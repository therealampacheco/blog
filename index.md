---
layout: default
title: Alejandro Pacheco
permalink: /
---

# Alejandro Guillermo Pacheco Masdíaz

## Resources to Share with Recruiters 
- [Alejandro's JSON Resume Online](https://registry.jsonresume.org/ampacheco)
- [Credly Profile](https://www.credly.com/users/ampacheco/badges)
- [Linkedin](https://linkedin.com/in/ampacheco)
- [Alejandro's Home](https://alejandropacheco.net)

## Certification Badges

### All Certifications (Credly Profile [🔗](https://credly.com/users/ampacheco/badges))

### - Business
[![INSEAD](https://images.credly.com/size/110x110/images/f1ce99d2-3d07-4704-9894-5b6a84d3ce1c/Financial_Acumen.png)](https://www.credly.com/earner/earned/badge/56ca160e-6ac6-4420-a1ed-62bcc1377555)

### - Microsoft

[![Solution Architect](https://images.credly.com/size/110x110/images/a5873bc2-5dc0-4f52-9337-cbf879219d82/MCSA_Cloud_Platform-01.png)](https://www.credly.com/badges/6f30fde0-6c91-4e1c-b4cb-103b981c20e4)
[![DevOps](https://images.credly.com/size/110x110/images/107e2eb6-f394-40eb-83d2-d8c9b7d34555/exam-az400-600x600.png)](https://www.credly.com/badges/4ef3c05f-9cdf-4243-ab6e-e3838b9e64ff)
[![MCT](https://images.credly.com/size/110x110/images/a6ea4416-4f34-4a85-bc24-eb3fe32fd241/MCT-Microsoft_Certified_Trainer-600x600.png)](https://credly.com/badges/5df524de-f199-4dc6-a5c7-9fca7d3c52b6)


[🔗 More from this Vendor](https://credly.com/users/ampacheco/badges)

### - Linux Foundations

[![CKAD](https://images.credly.com/size/110x110/images/f88d800c-5261-45c6-9515-0458e31c3e16/ckad_from_cncfsite.png)](https://www.credly.com/badges/31ddfb1d-333b-493e-a612-a426273c5d5d)


### - Hashicorp
[![Terrafrom](https://images.credly.com/size/110x110/images/5b075140-d286-4c8a-9be9-2b87f9e10839/Terraform-Associate-Badge.png)](https://www.credly.com/earner/earned/badge/23ceca26-a2f9-48ff-a4a4-44bab3d6c840)
[![Terrafrom](https://images.credly.com/size/110x110/images/7c6011a3-d6df-47b9-a4d6-7ab88d465296/Consul_Associate_Web__3_.png)](https://www.credly.com/earner/earned/badge/45365015-b7c0-443a-9b52-04c35d09348c)
